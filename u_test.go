package uploader

import (
	"gitee.com/jqllxew/simple/config"
	"github.com/go-resty/resty/v2"
	"github.com/kataras/golog"
	"net/http"
	"net/url"
	"testing"
)

func TestDownload(t *testing.T) {
	ck := &http.Cookie{
		Name:  "SessionId",
		Value: "xxxxxxxxxxxx",
	}
	r := resty.New().R()
	r.SetCookie(ck)
	rsp, err := DownloadReq(r, "http://xxxxxxxxxx")
	if err != nil {
		golog.Error(err)
	}
	filename := FilenameByRsp(rsp)
	unescape, err := url.QueryUnescape(filename)
	if err != nil {
		golog.Error(err)
	}
	golog.Info(unescape)
}
func TestCopy(t *testing.T) {
	config.Init("./u_test.yaml")
	ck := &http.Cookie{
		Name:  "SessionId",
		Value: "xx",
	}
	u, err := CopyObject("s/test", "http://xxxxxxxx", "", ck)
	if err != nil {
		golog.Error(err)
	}
	golog.Info(u)
}
