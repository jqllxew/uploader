package uploader

import (
	"gitee.com/jqllxew/simple/config"
	"gitee.com/jqllxew/simple/utils"
	"github.com/go-resty/resty/v2"
	"net/http"
	"regexp"
	"strings"
)

type Uploader interface {
	PutObject(key string, data []byte) (string, error)
	DeleteObject(key string) error
	GetHost() string
}
var (
	up Uploader
)
func generateKey(data []byte, suffix string) string {
	md5 := utils.Md5(data, "")
	return md5 + suffix
}
func getOrigin() Uploader {
	if up != nil {
		return up
	}
	ty := config.GetSimple().UploadType
	if ty == "local" {
		up = &LocalUploader{}
	} else if ty == "aliOss" {
		up = &AliOssUploader{
			Bucket: nil,
		}
	} else if ty == "txCos" {
		up = &TxCosUploader{
			Obj:    nil,
			Bucket: nil,
		}
	}
	return up
}
func SetOrigin(_up Uploader){
	if _up != nil{
		up = _up
	}
}
func PutObject(prefix, key string, data []byte) (string, error) {
	if prefix != "" {
		key = utils.UrlJoin(prefix, key)
	}
	return getOrigin().PutObject(key, data)
}
func DeleteObject(key string) error {
	return getOrigin().DeleteObject(key)
}
func CopyObject(prefix, originUrl, suffix string, ck *http.Cookie) (string, error) {
	req := resty.New().R()
	if ck != nil {
		req.SetCookie(ck)
	}
	rsp, err := DownloadReq(req, originUrl)
	if err != nil {
		return "", err
	}
	data := rsp.Body()
	if suffix == "" {
		suffix = SuffixByRsp(rsp)
	}
	key := generateKey(data, suffix)
	return PutObject(prefix, key, data)
}
func PutImage(prefix, key string, data []byte) (string, error) {
	if key == "" {
		key = generateKey(data, ".jpg")
	} else {
		key += ".jpg"
	}
	return PutObject(prefix, key, data)
}
func CopyImage(prefix, key, originUrl string) (string, error) {
	rsp, err := Download(originUrl)
	if err != nil {
		return "", err
	}
	return PutImage(prefix, key, rsp.Body())
}
func FilenameByRsp(rsp *resty.Response) string {
	header := rsp.Header().Get("Content-Disposition")
	reg, _ := regexp.Compile("filename=(.*)")
	filename := reg.FindString(header)
	return strings.Replace(filename, "filename=", "", 1)
}
func SuffixByRsp(rsp *resty.Response) string {
	filename := FilenameByRsp(rsp)
	return filename[strings.Index(filename, "."):]
}
func DownloadReq(req *resty.Request, url string) (*resty.Response, error) {
	return req.Get(url)
}
func Download(url string) (*resty.Response, error) {
	return resty.New().R().Get(url)
}
