package uploader

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/jqllxew/simple/config"
	"gitee.com/jqllxew/simple/utils"
	"github.com/tencentyun/cos-go-sdk-v5"
	"net/http"
	"net/url"
)

type TxCosUploader struct {
	Obj    *cos.ObjectService
	Bucket *cos.BucketService
}

func (tx *TxCosUploader) PutObject(key string, data []byte) (string, error) {
	if tx.Obj == nil || tx.Bucket == nil {
		tx.Obj, tx.Bucket = tx.getBucket()
	}
	_, err := tx.Obj.Put(context.Background(), key, bytes.NewReader(data), nil)
	if err != nil {
		return "", err
	}
	if config.GetSimple().UploadAddHost {
		return utils.UrlJoin(tx.GetHost(), key), nil
	}
	return key, err
}
func (tx *TxCosUploader) DeleteObject(key string) error {
	if tx.Obj == nil || tx.Bucket == nil {
		tx.Obj, tx.Bucket = tx.getBucket()
	}
	_, err := tx.Obj.Delete(context.Background(), key)
	return err
}
func (tx *TxCosUploader) GetHost() string {
	return config.Instance().TxCos.Host
}
func (tx *TxCosUploader) SetBucket(host, region, secretId, secretKey string) {
	u, _ := url.Parse(host)
	su, _ := url.Parse(fmt.Sprintf(`https://cos.%s.myqcloud.com`, region))
	b := &cos.BaseURL{BucketURL: u, ServiceURL: su}
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  secretId,
			SecretKey: secretKey,
		},
	})
	tx.Obj = client.Object
	tx.Bucket = client.Bucket
}
func (tx *TxCosUploader) getBucket() (*cos.ObjectService, *cos.BucketService) {
	var obj *cos.ObjectService
	var bucket *cos.BucketService
	conf := config.Instance().TxCos
	// 将 examplebucket-1250000000 和 COS_REGION修改为真实的信息
	u, _ := url.Parse(conf.Host)
	// 用于Get Service 查询，默认全地域 service.cos.myqcloud.com
	su, _ := url.Parse(fmt.Sprintf(`https://cos.%s.myqcloud.com`, conf.Region))
	b := &cos.BaseURL{BucketURL: u, ServiceURL: su}
	// 1.永久密钥
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  conf.SecretId,
			SecretKey: conf.SecretKey,
		},
	})
	obj = client.Object
	bucket = client.Bucket
	return obj, bucket
}
