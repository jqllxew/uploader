module gitee.com/jqllxew/uploader

go 1.16

require (
	gitee.com/jqllxew/simple v0.2.6
	github.com/aliyun/aliyun-oss-go-sdk v2.1.9+incompatible
	github.com/go-resty/resty/v2 v2.5.0
	github.com/kataras/golog v0.1.7
	github.com/tencentyun/cos-go-sdk-v5 v0.7.29
)
