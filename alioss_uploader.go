package uploader

import (
	"bytes"
	"gitee.com/jqllxew/simple/config"
	"gitee.com/jqllxew/simple/utils"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/kataras/golog"
)

type AliOssUploader struct {
	Bucket *oss.Bucket
}

func (ay *AliOssUploader) PutObject(key string, data []byte) (string, error) {
	if ay.Bucket == nil {
		ay.Bucket = ay.getBucket()
	}
	if err := ay.Bucket.PutObject(key, bytes.NewReader(data)); err != nil {
		return "", err
	}
	if config.GetSimple().UploadAddHost {
		return utils.UrlJoin(ay.GetHost(), key), nil
	}
	return key, nil
}
func (ay *AliOssUploader) DeleteObject(key string) error {
	if ay.Bucket == nil {
		ay.Bucket = ay.getBucket()
	}
	return ay.Bucket.DeleteObject(key)
}
func (ay *AliOssUploader) GetHost() string {
	return config.Instance().AliOss.Host
}
func (ay *AliOssUploader) SetBucket(endpoint, accessId, accessSecret, bucket string) {
	var b *oss.Bucket
	if client, err := oss.New(endpoint, accessId, accessSecret); err != nil {
		golog.Error(err)
	} else if b, err = client.Bucket(bucket); err != nil {
		golog.Error(err)
	}
	ay.Bucket = b
}
func (ay *AliOssUploader) FindKeys( prefix string) []string {
	var err error
	var ls oss.ListObjectsResult
	keys := make([]string, 0)
	marker := oss.Marker("")
	for {
		ls, err = ay.Bucket.ListObjects(oss.MaxKeys(500), marker, oss.Prefix(prefix))
		if err != nil {
			break
		}
		for _, o := range ls.Objects {
			keys = append(keys, o.Key)
		}
		marker = oss.Marker(ls.NextMarker)
		if !ls.IsTruncated {
			break
		}
	}
	return keys
}
func (ay *AliOssUploader) getBucket() *oss.Bucket {
	var bucket *oss.Bucket
	c := config.Instance().AliOss
	if client, err := oss.New(c.Endpoint, c.AccessId, c.AccessSecret); err != nil {
		golog.Error(err)
	} else if bucket, err = client.Bucket(c.Bucket); err != nil {
		golog.Error(err)
	}
	return bucket
}