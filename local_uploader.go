package uploader

import (
	"gitee.com/jqllxew/simple/config"
	"gitee.com/jqllxew/simple/utils"
	"io/ioutil"
	"os"
	"path/filepath"
)

// 本地文件
type LocalUploader struct{
	Path string
}

func (local *LocalUploader) PutObject(key string, data []byte) (string, error) {
	if err := os.MkdirAll("/", os.ModeDir); err != nil {
		return "", err
	}
	if local.Path == ""{
		local.Path = local.getPath()
	}
	filename := filepath.Join(local.Path, key)
	if err := os.MkdirAll(filepath.Dir(filename), os.ModePerm); err != nil {
		return "", err
	}
	if err := ioutil.WriteFile(filename, data, os.ModePerm); err != nil {
		return "", err
	}
	if config.GetSimple().UploadAddHost {
		return utils.UrlJoin(local.GetHost(), key), nil
	}
	return key, nil
}
func (local *LocalUploader) DeleteObject(key string) error {
	if local.Path == ""{
		local.Path = local.getPath()
	}
	filename := filepath.Join(local.Path, key)
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return os.Remove(filename)
	} else {
		return err
	}
}
func (local *LocalUploader) GetHost() string {
	return config.Instance().Local.Host
}
func (local *LocalUploader) SetPath(_path string) {
	local.Path = _path
}
func (local *LocalUploader) getPath() string {
	return config.Instance().Local.Path
}